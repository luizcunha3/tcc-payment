/**
 * Module Dependencies
 */
const errors = require('restify-errors');
const mongoose = require('mongoose');
const Payments = require('../models/payment');
const config = require('../config');

const getPayment = (req, res, next) => {
    if(req.params.id){
        return Payments.findById(req.params.id)
            .then(payment => {
                res.send(200, payment);
                next();
                return true
            })
            .catch(err => {
                console.error(err);
                next(err);
                return false;
            })
    } else {
        let query = {}
        if(req.params.name) {
            query.name = { $regex: '.*' + req.params.name + '.*', $options: 'i'}
        }
        if(req.params.kind){
            query.kind = { $regex: '.*' + req.params.kind + '.*', $options: 'i'}
        }

        return Payments.find(query)
            .then(payments => {
                res.send(200, payments);
                next();
                return true
            })
            .catch(err => {
                console.error(err);
                next(err);
                return false;
            })
    }
};

postPayment = (req, res, next) => {
    const payment = {
        name: req.body.name,
        value: req.body.value,
        datetime: req.body.datetime,
        kind: req.body.kind,
    };
    const paymentObj = new Payments(payment);
    return paymentObj.save()
        .then(result => {
            console.log(result);
            res.send(201);
            next();
            return true;
        })
        .catch(err => {
            console.error(err);
            next(err);
            return false
        })

}

const handleConnection = _ =>{
    return new Promise(((resolve, reject) => {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.db.uri, {useNewUrlParser: true});
        const db = mongoose.connection;

        db.on('error', (err) => {
           reject('Could not connect to mongo')
        });

        db.once('open', () => {
           resolve(true)
        });
    }))

};

module.exports = function(server) {
    server.get('/', (req, res, next) => {
        res.send(200, "Payment is running")
    });
    server.get('/payment', (req, res, next) => {
        handleConnection()
            .then(status => {
                return getPayment(req,res, next)
            })
            .then(res => {
                mongoose.disconnect()
            })
            .catch(err => {
                mongoose.disconnect()
                console.log(err)
            })
    });
    server.post('/payment', (req, res, next) => {
        handleConnection()
            .then(status => {
                return postPayment(req, res, next)
            })
            .then(res => {
                mongoose.disconnect()
            })
            .catch(err => {
                console.log(err);
                res.send(500);
            })
    });
};