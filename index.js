/**
 * Module Dependencies
 */
const config = require('./config');
const restify = require('restify');
const mongoose = require('mongoose');
const restifyPlugins = require('restify-plugins');

/**
 * Initialize Server
 */
const server = restify.createServer({
    name: config.name,
    version: config.version,
});

const connectWithRetry = options =>{
    return mongoose.connect(config.db.uri,options, (err => {
        if(err){
            console.log('Falhou a connexao');
            setTimeout(connectWithRetry,5000)
        }
    }))
}

/**
 * Middleware
 */
server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());

/**
 * Start Server, Connect to DB & Require Routes
 */
server.listen(config.port, () => {
    // establish connection to mongodb
    require('./routes')(server);
    console.log(`${config.name} is listening on port ${config.port}`);
});
