const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const PaymentSchema = new mongoose.Schema(
        {
            "name": {
                "type": "String"
            },
            "datetime": {
                "type": "String"
            },
            "value": {
                "type": "Number"
            },
            "kind": {
                "type": "String",
                enum: ['rental', 'theater']
            },
        }
    );
PaymentSchema.plugin(timestamps);
const PaymentModel = mongoose.model('payment', PaymentSchema, 'payment');
module.exports = PaymentModel;